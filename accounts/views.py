from django.contrib.auth import login
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.shortcuts import render, redirect

# Create your views here.


# user = User.objects.create_user()


def signup(request):
    if request.method == "POST":
        form = UserCreationForm(request.POST)
        if form.is_valid():
            person = request.POST.get("username")
            word = request.POST.get("password")
            user = User.objects.create_user(
                username=person,
                password=word,
            )
            user.save()
            login(request, user)
            return redirect("home")

    else:
        form = UserCreationForm()

    context = {
        "form": form,
    }
    return render(request, "registration/signup.html", context)
