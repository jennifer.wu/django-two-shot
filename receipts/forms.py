from receipts.models import Receipt


class CreateReceiptForm:
    class Meta:
        model = Receipt
        fields = ["vendor", "total", "tax", "date", "category", "account"]
